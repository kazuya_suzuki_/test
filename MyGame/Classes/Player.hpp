//
//  Player.hpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "MainScene.hpp"
#include "GameData.hpp"
#include "Fruits.hpp"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

// プレイヤークラス
class Player : public GameData
{
protected:
    // コンストタクタ
    Player();
    // デストラクタ
    virtual ~Player();
    // 初期化
    bool init() override;
    
public:
    // 更新
    void update( float dt ) override;
    
    CREATE_FUNC(Player);
    
    CC_SYNTHESIZE_RETAIN(cocos2d::Sprite *, _player, Player);
    
    // 爆弾
    CC_SYNTHESIZE(bool, _isCrash, IsCrash);
    
    // 爆弾に当たった時
    void onCrashBomb();
    
    // AIの移動（目標となるposを入れる）
    void AIMove( float targetPos );
    
    // プレイヤーのシングルトンを取得
    static Player *sharedPlayer();
    
    // フルーツクラスのシングルトン
    Fruits *fruits;
    
    // MainSceneのシングルトン
    MainScene *mainScene;
    
    // GameDataのシングルトン
    GameData *gameData;
    
    // 移動し終わるとtrue
    bool bIsMoveEnd = false;
    
    int a = 0;

};

#endif /* Player_hpp */
