//
//  GameData.cpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#include "GameData.hpp"
#include "cocos2d.h"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

// GameDataのシングルトン
static GameData* instanceGameData;

// コンストラクタ
GameData::GameData()
//: _fruitsBatchNode(NULL)
{
    instanceGameData = this;
}

// デストラクタ
GameData::~GameData()
{
    // フルーツノードの削除
    //CC_SAFE_RELEASE_NULL( _fruitsBatchNode );
}

// 初期化
bool GameData::init()
{
    if(!Layer::init()){
        return false;
    }
    
    return true;
}

// GameDataのシングルトンを取得
GameData *GameData::sharedGameData()
{
    return  instanceGameData;
}