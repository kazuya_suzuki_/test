//
//  Fruits.hpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#ifndef Fruits_hpp
#define Fruits_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <random>
#include "MainScene.hpp"
#include "GameData.hpp"
//#include "Player.hpp"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

class Fruits : public GameData{
    
protected:
    // コンストラクタ
    Fruits();
    // デストラクタ
    virtual ~Fruits();
    // 初期化
    bool init() override;
    
private:
    
    // 生成されたフルーツを返す
    Sprite* addFruit();
    
    // フルーツの削除
    bool removeFruit(cocos2d::Sprite *fruit);
    
    // 爆弾に当たった時
    //void onCrashBomb();
    
    // フルーツの移動
    Vec2 FruitMove( cocos2d::Sprite* fruits );
    
public:
    
//    //　フルーツの種類
//    enum class FruitsType
//    {
//        APPLE = 0,  // りんご
//        GRAPE,      // ぶどう
//        ORANGE,     // オレンジ
//        BANANA,     // バナナ
//        CHERRY,     // さくらんぼ
//        GOLDEN,     // 黄金のフルーツ
//        BOMB,       // 爆弾
//        COUNT       // 最大数
//    };
//    
//    // 画面の種類
//    enum class AriaType{
//        A = 0,  // 右
//        B,      // 真ん中
//        C,      // 右
//        COUNT   // エリア数
//    };
//    
//    // フルーツのデータをまとめた構造体
//    struct FruitData
//    {
//        FruitsType type;    // フルーツの種類
//        Vec2 pos;           // 生成された場所
//        AriaType aria;      // 生成されたエリア
//    };
    // フルーツのデータをまとめた構造体
    struct FruitData
    {
        FruitsType type;    // フルーツの種類
        Vec2 pos;           // 生成された場所
        AriaType aria;      // 生成されたエリア
        int createNumber;   // 生成された順番
    };

//    
//    // 各エリアごとの情報の構造体
//    struct AriaData{
//        AriaType type;  // エリアの種類
//        int FruitType[(int)FruitsType::COUNT];   // エリアごとのフルーツを格納
//    };

    // 更新
    void update(float dt) override;
    
    // フルーツの取得d
    void catchFruit(cocos2d::Sprite* fruit);
    
    // フルーツが生成された場所をAriaType型で取得
    AriaType GetAriaType( float x );
    
    // エリアごとの合計点数を調べ、一番高いエリアをAriaType型で返す
    AriaType GetTopAriaType();
    
    // 引数に入れた配列の中身を調べて合計点数を返す
    int GetAriaScore( int score[] );
    
    // フルーツの情報をListから取得
    Vec2 FruitDataListAnalysis();
    
    // 自動で生成される
    CREATE_FUNC(Fruits);
    
    // フルーツ用の配列を作成
    CC_SYNTHESIZE(cocos2d::Vector<cocos2d::Sprite *>, _fruits, Fruits);
    
    // 生成された順番
    CC_SYNTHESIZE(int, _createNumber, CreateNumber);
    
    // 爆弾
    //CC_SYNTHESIZE(bool, _isCrash, IsCrash);
    
    // クラッシュ状態
    CC_SYNTHESIZE(bool, _isCrashBomb, CrashBomb);
    
    // フルーツノード
    CC_SYNTHESIZE_RETAIN(SpriteBatchNode *, _fruitsBatchNode, FruitsBatchNode);

    // 乱数
    CC_SYNTHESIZE(std::mt19937, _engine, Engine);
    
    // 乱数を返す(minからmaxまでの乱数をfloatで返す)
    float generateRandom(float min, float max);
    
    // エリアごとの構造体
    AriaData ariaData[(int)AriaType::COUNT];
    
    // FruitData型の空のListを宣言
    std::list<FruitData> FruitDataList;
    
    // 現在いるエリア
    AriaType CurrentAriaType = AriaType::B;
    
    // 三分割した画面の大きさ
    float fOneAriaSize = 0;
    
    // AIがエリア移動する時true
    bool bIsAIAriaMove = false;
    
    // エリアごとの画面サイズのリスト
    float fAriaSizeList[(int)AriaType::COUNT];
    
    // フルーツクラスのシングルトンを取得
    static Fruits *sharedFruits();
    
    // MainSceneのシングルトン
    MainScene *mainScene;
    
    // GameDataのシングルトン
    GameData *gameData;
    
    // クラッシュ状態
    //bool bIsCrash = false;
    int a = 0;
    
};

#endif /* Fruits_hpp */
