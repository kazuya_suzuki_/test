//
//  MainScene.hpp
//  MyGame
//
//  Created by H1-200 on 2015/12/07.
//
//

#ifndef MainScene_hpp
#define MainScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <random>
#include <vector>
//#include <list>

// メインシーン
class MainScene : public cocos2d::Layer
{
protected:
    // コンストラクタ
    MainScene();
    // デストラクタ
    virtual ~MainScene();
    // 初期化
    bool init() override;
    
private:
//    //　フルーツの種類
//    enum class FruitsType
//    {
//        APPLE = 0,  // りんご
//        GRAPE,      // ぶどう
//        ORANGE,     // オレンジ
//        BANANA,     // バナナ
//        CHERRY,     // さくらんぼ
//        GOLDEN,     // 黄金のフルーツ
//        BOMB,       // 爆弾
//        COUNT       // 最大数
//    };
    
    
//    // 画面の種類
//    enum class AriaType{
//        A = 0,
//        B,
//        C,
//        COUNT
//    };
//    
//    // フルーツのデータをまとめた構造体
//    struct FruitData
//    {
//        FruitsType type;    // フルーツの種類
//        cocos2d::Vec2 pos;           // 生成された場所
//        AriaType aria;      // 生成されたエリア
//    };
//    
//    // 各エリアごとの情報の構造体
//    struct AriaData{
//        AriaType type;  // エリアの種類
//        int FruitType[(int)FruitsType::COUNT];   // エリアごとのフルーツを格納
//    };
    
//    // 生成されたフルーツを返す
//    cocos2d::Sprite* addFruit();
//    
//    // フルーツの削除
//    bool removeFruit(cocos2d::Sprite *fruit);
//    
//    // フルーツの取得
//    void catchFruit(cocos2d::Sprite* fruit);
//    
//    // 爆弾に当たった時
//    void onCrashBomb();
    
    // ゲーム開始の文字
    void addReadyLabel();
    
    // フルーツの移動
    //cocos2d::Vec2 FruitMove( cocos2d::Sprite* fruits );
    
//    // フルーツが生成された場所をAriaType型で取得
//    AriaType GetAriaType( float x );
//    
//    // エリアごとの合計点数を調べ、一番高いエリアをAriaType型で返す
//    //AriaType GetTopAriaType( AriaData ariaScore[(int)AriaType::COUNT] );
//    AriaType GetTopAriaType();
//    
//    // 引数に入れた配列の中身を調べて合計点数を返す
//    int GetAriaScore( int score[] );
//    
//    // フルーツの情報をListから取得
//    //cocos2d::Vec2 FruitDataListAnalysis( FruitData fruitData );
//    cocos2d::Vec2 FruitDataListAnalysis();
//    
//    // 引数に入れた配列の中身から対応したフルーツを削除
//    //void AriaRemoveFruit( std::vector<int> vec, int type );
    
public:
    
    // ゲームの状態
    enum class GameState
    {
        READY = 0,  // 開始前
        PLAYING,    // ゲーム中
        ENDING,     // 終了演出中
        RESULT      // リザルト
    };

    
    // シーン作成
    static cocos2d::Scene* createScene();
    
    // 更新
    void update(float dt) override;
    
    // サウンドを鳴らす(トランジション（シーン移行）が終了したタイミングで一度だけ呼ばれる)
    void onEnterTransitionDidFinish() override;
    
    // 自動でcreateされる
    CREATE_FUNC(MainScene);
    
  //Users/H1-200/Documents/MyGame/Classes/AppDelegate.cpp
    // _player変数とgetPlayer()メソッド、setPlayer(Sprite *)メソッドが自動的に実装される
    //CC_SYNTHESIZE_RETAIN(cocos2d::Sprite *, _player, Player);
    
    // フルーツ用の配列を作成
    //CC_SYNTHESIZE(cocos2d::Vector<cocos2d::Sprite *>, _fruits, Fruits);
    
    // 点数を格納
    CC_SYNTHESIZE(int, _score, Score);
    
    // 点数を表示するLabelを格納
    CC_SYNTHESIZE_RETAIN(cocos2d::Label *, _scoreLabel, ScoreLabel);
    
    // 残り時間を格納
    CC_SYNTHESIZE(float, _second, Second);
    
    // 残り時間を表示するlabelを格納
    CC_SYNTHESIZE_RETAIN(cocos2d::Label *, _secondLabel, SecondLabel);
    
    // 爆弾
    //CC_SYNTHESIZE(bool, _isCrash, IsCrash);
    
    // ゲームの状態
    CC_SYNTHESIZE(GameState, _state, State);
    
    // フルーツノード
    //CC_SYNTHESIZE_RETAIN(cocos2d::SpriteBatchNode *, _fruitsBatchNode, FruitsBatchNode);
    
    // 乱数
    //CC_SYNTHESIZE(std::mt19937, _engine, Engine);
    
    // リザルトが呼ばれる
    void onResult();
    
    // 乱数を返す(minからmaxまでの乱数をfloatで返す)
    float generateRandom(float min, float max);

//    // エリアごとの構造体
//    AriaData ariaData[(int)AriaType::COUNT];
//    
//    // FruitData型の空のListを宣言
//    std::list<FruitData> FruitDataList;
//    
//    // 現在いるエリア
//    AriaType CurrentAriaType = AriaType::B;
//    
//    // 三分割した画面の大きさ
//    float fOneAriaSize = 0;
//    
//    // AIがエリア移動する時true
//    bool bIsAIAriaMove = false;
//    
//    // エリアごとの画面サイズのリスト
//    float fAriaSizeList[(int)AriaType::COUNT];
//    
//    // AIの移動（目標となるposを入れる）
//    void AIMove( float targetPos );
    
    // MainSceneのシングルトン
    static MainScene *sharedMainScene();

    // フルーツに当たったフラグ
    bool bIsCatch = false;
};



#endif /* MainScene_hpp */
