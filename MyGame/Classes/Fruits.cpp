//
//  Fruits.cpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#include "Fruits.hpp"
#include "MainScene.hpp"
#include "SimpleAudioEngine.h"
//#include "Player.hpp"
#include "GameData.hpp"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

const int FRUIT_TOP_MARGIN = 40;    // フルーツの出現位置（高さ）
const int NOLMAL_FRUIT_SCORE = 1;   // 普通のフルーツの点数
const int GOLDEN_FRUIT_SCORE = 10;   // 黄金フルーツの点数
const int BOMB_PENALTY_SCORE = 4;   // 爆弾を取ってしまった時のマイナス点
const float GOLDEN_FRUIT_PROBABILITY_BASE = 0.05;   // 黄金フルーツが出る確率の初期値
const float BOMB_PROBABILITY_BASE = 0.07;   // 爆弾が出る各区率の初期値
const int NOLMAL_FRUIT_COUNT = 5;   // 普通のフルーツの数

// フルーツごとの各点数を入っている配列
int FruitScore[] = {
    NOLMAL_FRUIT_SCORE, // りんご
    NOLMAL_FRUIT_SCORE, // ぶどう
    NOLMAL_FRUIT_SCORE, // オレンジ
    NOLMAL_FRUIT_SCORE, // バナナ
    NOLMAL_FRUIT_SCORE, // さくらんぼ
    GOLDEN_FRUIT_SCORE, // 黄金のフルーツ
    -BOMB_PENALTY_SCORE  // 爆弾
};

// フルーツのシングルトン
static Fruits* instanceFruits;

// コンストラクタ
Fruits::Fruits()
: _isCrashBomb(false),
_fruitsBatchNode(NULL),
_createNumber(0)
{
    // 乱数の初期化
    std::random_device rdev;
    _engine.seed(rdev());
}

// デストラクタ
Fruits::~Fruits()
{
    // フルーツノードの削除
    CC_SAFE_RELEASE_NULL( _fruitsBatchNode );
}

// 初期化
bool Fruits::init()
{
    if(!Layer::init()){
        return false;
    }
    
    // Directorを取り出す
    auto director = Director::getInstance();
    // 画面サイズを取り出す
    auto size = director->getWinSize();
    // 画面を三分割したサイズ
    fOneAriaSize = size.width / 3;
    
    // 画面サイズのリスト
    fAriaSizeList[(int)AriaType::A] = fOneAriaSize;   // A
    fAriaSizeList[(int)AriaType::B] = fOneAriaSize * 2;   // B
    fAriaSizeList[(int)AriaType::C] = fOneAriaSize * 3;   // C
    
    // 構造体の初期化（これで構造体のメンバーが全て０で初期化される）
    struct AriaData ariaDataCreate = {(AriaType)0};
    ariaData[(int)AriaType::A] = ariaDataCreate;
    ariaData[(int)AriaType::B] = ariaDataCreate;
    ariaData[(int)AriaType::C] = ariaDataCreate;

    // BatchNodeの初期化
    auto fruitsBatchNode = SpriteBatchNode::create("fruits.png");
    this->addChild(fruitsBatchNode);
    this->setFruitsBatchNode(fruitsBatchNode);
    
    // フルーツクラスのシングルトンを設定
    instanceFruits = this;
    
    // MainSceneクラスのシングルトン取得
    mainScene = MainScene::sharedMainScene();
    
    gameData = GameData::sharedGameData();

    
    // 更新
    this->scheduleUpdate();
    
    return true;
}

// 更新
void Fruits::update(float dt)
{
    // ゲームプレイ中なら
    if( mainScene->getState() == MainScene::GameState::PLAYING ){
        // フルーツの出現
        float RandomFruit = 0.8;
        float random = this->generateRandom( 0, 1 );
        if( random < RandomFruit ){
            this->addFruit();
        }
    
        //auto fruitData = gameData->getFruits();
        for(auto& fruit : _fruits){
            //log("s");
            // フルーツの移動
            auto pos = FruitMove( fruit );
            fruit->setPosition(pos);
            
            // 当たり判定
//            Vec2 busketPosition = _player->getPosition() - Vec2(0,10);
//            // Spriteの描画範囲を取得
//            Rect boundingBox = fruit->getBoundingBox();
//            bool isHit = boundingBox.containsPoint(busketPosition);
//            if(isHit){
//                this->catchFruit(fruit);
//            }

        }
    }
}

// 乱数を返す
float Fruits::generateRandom(float min, float max)
{
    std::uniform_real_distribution<float> dest(min,max);
    return dest(_engine);
}

// 生成したフルーツを返す
Sprite* Fruits::addFruit()
{
    // 画面サイズ取得
    auto winSize = Director::getInstance()->getWinSize();

    // フルーツの生成 -------------------------
    // フルーツの種類を選択
    int fruitType = 0;  // フルーツの種類を保持
    float r = this->generateRandom(0, 1);
    // 黄金フルーツの出現率
    float goldenFruitProbability = GOLDEN_FRUIT_PROBABILITY_BASE;
    float bombProbability = BOMB_PROBABILITY_BASE;

    // 求めた確率が黄金フルーツだったら
    if(r <= goldenFruitProbability){
        fruitType = static_cast<int>(FruitsType::GOLDEN);
    }
    // 求めた確率が爆弾だったら
    else if(r <= goldenFruitProbability + bombProbability){
        fruitType = static_cast<int>(FruitsType::BOMB);
    }
    // 求めた確率が普通のフルーツなら
    else{
        fruitType = round(this->generateRandom(0, NOLMAL_FRUIT_COUNT - 1));
    }

    // テクスチャのサイズを取り出す
    auto textureSize = _fruitsBatchNode->getTextureAtlas()->getTexture()->getContentSize();
    // テクスチャの横幅を個数で割るとフルーツ一つの幅になる
    auto fruitWidth = textureSize.width / static_cast<int>(FruitsType::COUNT);
    Sprite* fruit = Sprite::create("fruits.png",Rect(fruitWidth * fruitType,
                                                  0,
                                                  fruitWidth,
                                                  textureSize.height));
    fruit->setTag(fruitType);  // フルーツの種類をタグとして指定
    auto num = StringUtils::toString(_createNumber);
    fruit->setName(num);    // フルーツに名前をつける
    

    auto fruitSize = fruit->getContentSize();   //フルーツのサイズを取得
    float fruitXPos = rand() % static_cast<int>(winSize.width);    //X軸のランダムな位置を計算

    float fFruitYPos = winSize.height - FRUIT_TOP_MARGIN - fruitSize.height / 2.0f;
    fruit->setPosition(Vec2(fruitXPos, fFruitYPos));

    // フルーツが生成された場所からエリアを決定
    auto ariaType = GetAriaType(fruitXPos);

    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
    ariaData[(int)ariaType].FruitType[fruitType] += FruitScore[(int)fruitType];

    // BatchNodeにフルーツを追加
    _fruitsBatchNode->addChild(fruit);
    
    // フルーツの情報を構造体の変数にpushback
    FruitData fruitData;
    fruitData.type = (FruitsType)fruit->getTag();
    fruitData.pos = Vec2(fruitXPos, fFruitYPos);
    fruitData.aria = GetAriaType(fruitXPos);
    fruitData.createNumber = _createNumber++;
    FruitDataList.push_back(fruitData);

    _fruits.pushBack(fruit);    // ベクター配列にフルーツを追加
    // -------------------------------------

    return fruit;
}

// フルーツの削除
bool Fruits::removeFruit(Sprite *fruit)
{

    //log("削除%d",a++);
    // Directorを取り出す
    auto director = Director::getInstance();
    // 画面サイズを取り出す
    auto size = director->getWinSize();
    
    auto name = fruit->getName();
    //log("削除番号 : %s",name.c_str());
    // 画面から消えたフルーツを消す
    std::list<FruitData>::iterator fruitIterator;
    fruitIterator = FruitDataList.begin();
    //while (fruitIterator != FruitDataList.end()) {
    for( auto fruitData : FruitDataList ){
        auto num = StringUtils::toString(fruitData.createNumber);
        if(num == name){
            //log("num : %s",num.c_str());
            //log("name : %s",name.c_str());
            fruitIterator = FruitDataList.erase(fruitIterator);
        }else++fruitIterator;
//        else if(fruitData.pos.y <= size.height - 445){
//            fruitIterator = FruitDataList.erase(fruitIterator);
//        }else++fruitIterator;
        
    }
    
    // 当たった場所からエリアを決定
    auto ariaType = GetAriaType( fruit->getPosition().x );
    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
    ariaData[(int)ariaType].FruitType[fruit->getTag()] -= 1;

    // _fruits(フルーツを格納している配列)にfruit(引数に入っていきたフルーツ)が含まれているか
    if(_fruits.contains(fruit)){
        // 親のノードから削除
        fruit->removeFromParent();
        // _fruits配列から削除
        _fruits.eraseObject(fruit);
        return true;
    }
    return false;
}



// フルーツに当たったら
void Fruits::catchFruit(Sprite *fruit){

    // もしクラッシュしていたら、フルーツを取得できない
    if(this->getCrashBomb()){
        return;
    }

    auto audioEngin = CocosDenshion::SimpleAudioEngine::getInstance();
    // フルーツのタイプを取得
    FruitsType fruittype = static_cast<FruitsType>(fruit->getTag());
    
    auto score = mainScene->getScore();

    switch (fruittype) {
        // 黄金フルーツの場合
        case FruitsType::GOLDEN:
            score += GOLDEN_FRUIT_SCORE;
            mainScene->setScore(score);
            audioEngin->playEffect("catch_golden.mp3");

            break;

        // 爆弾の場合
        case FruitsType::BOMB:
            _isCrashBomb = true;
            score = MAX(0,score - 4);
            mainScene->setScore(score);
            audioEngin->playEffect("catch_bomb.mp3");
            break;

        // それ以外のフルーツの場合
        default:
            score += 1;
            mainScene->setScore(score);
            audioEngin->playEffect("catch_fruit.mp3");
            break;
    }

    // 当たった場所からエリアを決定
    auto ariaType = GetAriaType( fruit->getPosition().x );
    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
    ariaData[(int)ariaType].FruitType[(int)fruittype] += -(FruitScore[(int)fruittype]);

    if(fruit != NULL)
    {
        //通常の処理　remove();
        // フルーツの削除
        removeFruit(fruit);
        log("ID : %dが削除されます", fruit->getTag());
    }
    else
    {
        log("ID : %dが存在しません", fruit->getTag());
    }
    
}

// フルーツの移動
Vec2 Fruits::FruitMove( cocos2d::Sprite* fruits )
{

    auto FruitPos = fruits->getPosition();
    //log("取得したフルーツの座標　X : %f Y : %f", FruitPos.x, FruitPos.y);
    FruitPos.y -= 1;

//    // フルーツの情報を構造体の変数にpushback : ここの処理は毎フレームやってしまうとまずいから改善する
//    FruitData fruitData;
//    fruitData.type = (FruitsType)fruits->getTag();
//    fruitData.pos = FruitPos;
//    fruitData.aria = GetAriaType(FruitPos.x);
//    FruitDataList.push_back(fruitData);

    // Directorを取り出す
    auto director = Director::getInstance();
    // 画面サイズを取り出す
    auto size = director->getWinSize();

//    // 画面から消えたフルーツを消す : ここの処理は毎フレームやってしまうとまずいから改善する
//    std::list<FruitData>::iterator fruitIterator;
//    fruitIterator = FruitDataList.begin();
//    while (fruitIterator != FruitDataList.end()) {
//        if(fruitData.pos.y <= size.height - 445){
//            fruitIterator = FruitDataList.erase(fruitIterator);
//        }else++fruitIterator;
//        
//    }


//    std::list<FruitData>::iterator fruitIterator;
//    fruitIterator = FruitDataList.begin();
//    fruitIterator++;
//    FruitDataList.remove_if(fruitData.pos.y < size.height - 455 );
//    for( auto fruitData : FruitDataList ){
//        // フルーツが画面から消えたら
//        if(fruitData.pos.y <= size.height - 445){
//            FruitDataList.remove(fruitData);
//            //log("通ってるよー");
//        }
//    }


    if(fruits != NULL){
        // 画面から消えたら
        if(FruitPos.y < 0){
            // 当たった場所からエリアを決定
            auto ariaType = GetAriaType( fruits->getPosition().x );
            // フルーツ生成と同時に生成したフルーツに対応したデータを加算
            ariaData[(int)ariaType].FruitType[fruits->getTag()] += -(FruitScore[fruits->getTag()]);
            
            // removeFruitを即座に呼びだづアクション
            auto remove = CallFuncN::create([this](Node *node){
                //NodeをSpriteにダウンキャスト
                auto sprite = dynamic_cast<Sprite *>(node);
                
                // removeFruitを呼びだす
                this->removeFruit(sprite);
            });
            fruits->runAction(remove);
        }

    }
    else{
        log("ID : %dが存在しません", fruits->getTag());
    }
    return FruitPos;
}

// フルーツが生成された場所をAriaType型で取得
Fruits::AriaType Fruits::GetAriaType( float x )
{
    // 最終的にreturnする値
    AriaType aria;

    // 引数でもらったposと画面サイズを比較して対応したAriaTypeをいれる
    // A
    if(x < fAriaSizeList[(int)AriaType::A]){
        aria = AriaType::A;
    }

    // B
    else if(x >= fAriaSizeList[(int)AriaType::A] && x < fAriaSizeList[(int)AriaType::B]){
        aria = AriaType::B;
    }

    // C
    else if(x >= fAriaSizeList[(int)AriaType::B] && x < fAriaSizeList[(int)AriaType::C]){
        aria = AriaType::C;
    }

    return aria;
}

// エリアごとの合計点数を調べ、一番高いエリアをAriaType型で返す
Fruits::AriaType Fruits::GetTopAriaType()
{
    // 各エリアのスコアを取得して配列にする
    int AriaScoreList[] = {
        GetAriaScore( ariaData[(int)AriaType::A].FruitType ), // A
        GetAriaScore( ariaData[(int)AriaType::B].FruitType ), // B
        GetAriaScore( ariaData[(int)AriaType::C].FruitType )  // C
    };

    //log("点数A : %d, 点数B : %d, 点数C : %d", AriaScoreList[0],AriaScoreList[1],AriaScoreList[2]);

    // 一番スコアがいいエリアを決める
    auto Top = MAX( (AriaScoreList[(int)AriaType::A]), (AriaScoreList[(int)AriaType::B]) );
    auto TopScore = MAX(Top, AriaScoreList[(int)AriaType::C]);

    // 点数の差の基準
    const int difference = 5;
    // 点数の差
    auto scoreLenght = TopScore - AriaScoreList[(int)CurrentAriaType];
    // 他のエリアとの差が大きかったらエリア移動のフラグをtrue
    if( scoreLenght > difference ){
        bIsAIAriaMove = true;
    }
    else{
        bIsAIAriaMove = false;
    }

    // 一番点数の高いエリアを入れる（初期はBにいるのでBを入れてる）
    AriaType TopScoreAriaType = AriaType::B;
    for( int i = 0; i < (int)AriaType::COUNT; i++ ){
        // 一番高いスコアと一致したら
        if( TopScore == AriaScoreList[i] ){
            // AriaTypeは中身はintになってるのでiをそのまま代入
            TopScoreAriaType = (AriaType)i;
        }
    }

    return TopScoreAriaType;
}


// 引数に入れた配列の中身を調べて合計点数を返す
int Fruits::GetAriaScore( int score[] )
{

    // エリアの点数
    auto AriaScore = 0;

    // 種類別の点数を入れる
    for( int i = 0; i < (int)FruitsType::COUNT; i++ ){
        AriaScore += score[i];
    }
    return AriaScore;
}

// フルーツの情報をListから取得
Vec2 Fruits::FruitDataListAnalysis()
{
    // フルーツの座標を保持する
    //Vec2 fruitPos = Vec2(0,100000);
    Vec2 fruitPos = Vec2(0,0);

    int number = 10000;

    // fruitData分だけ
    for( auto fruitData : FruitDataList ){
        // フルーツの場所が今いるエリアだったら
        if(fruitData.aria == CurrentAriaType){
            //log("中身 : %f", fruitData.pos.y);
            if(fruitData.createNumber < number && fruitData.type != FruitsType::BOMB){
                number = fruitData.createNumber;
                fruitPos = fruitData.pos;
            }
        }
    }

    return fruitPos;
}

// フルーツクラスのシングルトン取得
Fruits *Fruits::sharedFruits()
{
    return instanceFruits;
}


