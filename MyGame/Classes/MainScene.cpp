//
//  MainScene.cpp
//  MyGame
//
//  Created by H1-200 on 2015/12/07.
//
//

#include "SimpleAudioEngine.h"
#include "MainScene.hpp"
#include "Fruits.hpp"
#include "Player.hpp"
//#include "HelloWorldScene.h"
#include "TitleScene.hpp"
#include <iostream>
#include <vector>
#include <List>

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)


//const int FRUIT_TOP_MARGIN = 40;    // フルーツの出現位置（高さ）
//const int FRUIT_SPAWN_RATE = 20;    // フルーツの出現頻度
const float TIME_LIMIT_SECOND = 600; // 制限時間
//const int NOLMAL_FRUIT_SCORE = 1;   // 普通のフルーツの点数
//const int GOLDEN_FRUIT_SCORE = 5;   // 黄金フルーツの点数
//const int BOMB_PENALTY_SCORE = 4;   // 爆弾を取ってしまった時のマイナス点
//const float GOLDEN_FRUIT_PROBABILITY_BASE = 0.02;   // 黄金フルーツが出る確率の初期値
//const float BOMB_PROBABILITY_BASE = 0.05;   // 爆弾が出る各区率の初期値
//const float GOLDEN_FRUIT_PROBABILITY_RATE = 0.001;  // 黄金フルーツが出る確率の増え幅
//const float BOMB_PROBABILITY_RATE = 0.003;  // 爆弾が出る確率の増え幅
//const int NOLMAL_FRUIT_COUNT = 5;   // 普通のフルーツの数
//const float FRUIT_SPAWN_INCREASE_BASE = 0.02;   // フルーツ出現頻度の初期値
//const float FRUIT_SPAWN_INCREASE_RATE = 1.05;   // フルーツ出現現頻度の増加量
//const float MAXMUM_SPAWN_PROBABILITY = 0.5;     // フルーツ出現頻度の最大値

//// フルーツごとの各点数を入っている配列
//int FruitScore[] = {
//    NOLMAL_FRUIT_SCORE, // りんご
//    NOLMAL_FRUIT_SCORE, // ぶどう
//    NOLMAL_FRUIT_SCORE, // オレンジ
//    NOLMAL_FRUIT_SCORE, // バナナ
//    NOLMAL_FRUIT_SCORE, // さくらんぼ
//    GOLDEN_FRUIT_SCORE, // 黄金のフルーツ
//    -BOMB_PENALTY_SCORE  // 爆弾
//};

// MainSceneのシングルトン
static MainScene* instanceMainScene;


// コンストラクタ
MainScene::MainScene()
: //_player(NULL),
_score(0),
_scoreLabel(NULL),
_second(TIME_LIMIT_SECOND),
_secondLabel(NULL),
_state(GameState::READY)
//_isCrash(false),
//_fruitsBatchNode(NULL)
{
//    // 乱数の初期化
//    std::random_device rdev;
//    _engine.seed(rdev());
}

// デストラクタ
MainScene::~MainScene()
{
    // プレイヤーを削除
    //CC_SAFE_RELEASE_NULL(_player);
    // スコアラベルの削除
    CC_SAFE_RELEASE_NULL(_scoreLabel);
    
    // 制限時間のラベルを削除
    CC_SAFE_RELEASE_NULL(_secondLabel);
    
    // フルーツノードの削除
    //CC_SAFE_RELEASE_NULL(_fruitsBatchNode);
}

// シーン生成
Scene* MainScene::createScene()
{
    auto scene = Scene::create();
    
    // MainSceneクラスを紐づける
    auto layer = MainScene::create();
    scene->addChild(layer);
    
    // Fruitsクラスを紐づける
    auto fruits = Fruits::create();
    scene ->addChild(fruits);
    
    // Playerクラスを紐づける
    auto player = Player::create();
    scene ->addChild(player);
    
    auto gameData = GameData::create();
    scene->addChild(gameData);
    
    
    
    return scene;
}

// 初期化
bool MainScene::init()
{
    if(!Layer::init()){
        return false;
    }
    
    // MainSceneのシングルトン
    instanceMainScene = this;
    
    // Directorを取り出す
    auto director = Director::getInstance();
    // 画面サイズを取り出す
    auto size = director->getWinSize();
//    // 画面を三分割したサイズ
//    fOneAriaSize = size.width / 3;
//    
//    // 画面サイズのリスト
//    fAriaSizeList[(int)AriaType::A] = fOneAriaSize;   // A
//    fAriaSizeList[(int)AriaType::B] = fOneAriaSize * 2;   // B
//    fAriaSizeList[(int)AriaType::C] = fOneAriaSize * 3;   // C
//
//    // 構造体の初期化（これで構造体のメンバーが全て０で初期化される）
//    struct AriaData ariaDataCreate = {(AriaType)0};
//    ariaData[(int)AriaType::A] = ariaDataCreate;
//    ariaData[(int)AriaType::B] = ariaDataCreate;
//    ariaData[(int)AriaType::C] = ariaDataCreate;
    
    
    
    // 背景の配置 ----------------------------
    // 背景のスプライトを生成する
    auto background = Sprite::create("background.png");
    // スプライトの表示位置
    background->setPosition(Vec2(size.width / 2.0, size.height / 2.0));
    //background->setScale(2.0f, 2.0f);
    // 親のノードにスプライトを追加
    this->addChild(background);
    // -------------------------------------
    
    // プレイヤーの位置 -----------------------
//    this->setPlayer(Sprite::create("player.png"));  // スプライトを生成して格納
//    _player->setPosition(Vec2(size.width / 2.0f, size.height - 445)); // プレイヤーの位置
//    this->addChild(_player);    // シーンにプレイヤーを配置
    // -------------------------------------
    
//    // タッチ判定　---------------------------
//    // タッチされた時の処理
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->onTouchBegan = [](Touch* touch, Event* event){
//        // Directorを取り出す
//        auto director = Director::getInstance();
//        // 画面サイズを取り出す
//        auto size = director->getWinSize();
//        //log("タッチされた瞬間の座標 (%f,%f)", touch->getLocation().x, touch->getLocation().y);
//        return true;
//    };
//    // タッチ中に動いた時の処理
//    listener->onTouchMoved = [this](Touch* touch, Event* event){
//        if(!this->getIsCrash()){
//            // 前回とのタッチ位置との差をベクトルで取得
//            Vec2 delta = touch->getDelta();
//            
//            // 現在のプレイヤーの位置を取得
//            Vec2 position = _player->getPosition();
//            
//            // 現在の座標 + 移動量を計算
//            Vec2 newPosition = position + delta;
//            
//            //log("プレイヤーのx : %f" , newPosition.x);
//            //log("プレイヤーのy : %f" , newPosition.y);
//            
//            // 画面サイズを取り出す
//            auto winSize = Director::getInstance()->getWinSize();
//            // 画面外に出ないようにする処理
//            if(newPosition.x < 0){
//                newPosition.x = 0;
//            }
//            else if(newPosition.x > winSize.width){
//                newPosition.x = winSize.width;
//            }
//            
//            // Directorを取り出す
//            auto director = Director::getInstance();
//            // 画面サイズを取り出す
//            auto size = director->getWinSize();
//            newPosition.y = size.height - 445;
//            
//            // Playerの座標を更新
//            _player->setPosition(newPosition);
//        }
//        
//        
//        return true;
//    };
//    // タッチが離された時の処理
//    listener->onTouchEnded = [](Touch* touch, Event* event){
//        
//        return true;
//    };
//    // タッチがキャンセルされた時の処理
//    listener->onTouchCancelled = [](Touch* touch, Event* event){
//        
//        return true;
//    };
//    // EventListenerを登録
//    director->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
//    
//    // -------------------------------------
    
    // スコアラベル追加 -----------------------
    // 表示する文字、フォント、フォントサイズ
    auto scoreLabel = Label::createWithSystemFont(StringUtils::toString(_score), "Marker Felt", 16);
    this->setScoreLabel(scoreLabel);
    // 影を有効にする
    scoreLabel->enableShadow(Color4B::BLACK, Size(0.5,0.5), 3);
    // アウトラインの設定
    scoreLabel->enableOutline(Color4B::BLACK, 1.5);
    scoreLabel->setPosition(Vec2(size.width /2.0 * 1.5, size.height - 40));
    this->addChild(_scoreLabel);
    // -------------------------------------
    
    // スコアヘッダーの追加　-------------------
    // 表示する文字、フォント、フォントサイズ
    auto scoreLabelHeader = Label::createWithSystemFont("SCORE", "Marker Felt", 16);
    // 影を有効にする
    scoreLabelHeader->enableShadow(Color4B::BLACK, Size(0.5,0.5), 3);
    // アウトラインの設定
    scoreLabelHeader->enableOutline(Color4B::BLACK, 1.5);
    scoreLabelHeader->setPosition(Vec2(size.width /2.0 * 1.5, size.height - 20));
    this->addChild(scoreLabelHeader);
    // -------------------------------------
    
    // タイマーラベルの追加 -------------------
    int second = static_cast<int>(_second);
    auto secondLabel = Label::createWithSystemFont(StringUtils::toString(second), "Marker Felt", 16);
    this->setSecondLabel(secondLabel);
    // 影を有効にする
    secondLabel->enableShadow(Color4B::BLACK, Size(0.5,0.5),3);
    // アウトラインの設定
    secondLabel->enableOutline(Color4B::BLACK, 1.5);
    secondLabel->setPosition(Vec2(size.width /2.0, size.height - 40));
    this->addChild(secondLabel);
    // -------------------------------------
    
    // スコアヘッダーの追加　-------------------
    // 表示する文字、フォント、フォントサイズ
    auto secondLabelHeader = Label::createWithSystemFont("TIME", "Marker Felt", 16);
    // 影をsecondLabelHeaderにする
    scoreLabelHeader->enableShadow(Color4B::BLACK, Size(0.5,0.5), 3);
    // アウトラインの設定
    secondLabelHeader->enableOutline(Color4B::BLACK, 1.5);
    secondLabelHeader->setPosition(Vec2(size.width /2.0, size.height - 20));
    this->addChild(secondLabelHeader);
    // -------------------------------------

//    // BatchNodeの初期化
//    auto fruitsBatchNode = SpriteBatchNode::create("fruits.png");
//    this->addChild(fruitsBatchNode);
//    this->setFruitsBatchNode(fruitsBatchNode);
    
    // 更新
    this->scheduleUpdate();
    
    return true;
}

// 生成されたフルーツを返す
//Sprite* MainScene::addFruit()
//{
//    // 画面サイズ取得
//    auto winSize = Director::getInstance()->getWinSize();
//    
//    // フルーツの生成 -------------------------
//    // フルーツの種類を選択
//    int fruitType = 0;  // フルーツの種類を保持
//    float r = this->generateRandom(0, 1);
//    int pastSecond = TIME_LIMIT_SECOND - _second;   // 経過時間
//    // 初期出現率　＋　出現率の増加量　＊　経過秒数
//    float goldenFruitProbability = GOLDEN_FRUIT_PROBABILITY_BASE + GOLDEN_FRUIT_PROBABILITY_RATE * pastSecond;
//    float bombProbability = BOMB_PROBABILITY_BASE + BOMB_PROBABILITY_RATE * pastSecond;
//    
//    // 求めた確率が黄金フルーツだったら
//    if(r <= goldenFruitProbability){
//        fruitType = static_cast<int>(FruitsType::GOLDEN);
//    }
//    // 求めた確率が爆弾だったら
//    else if(r <= goldenFruitProbability + bombProbability){
//        fruitType = static_cast<int>(FruitsType::BOMB);
//    }
//    // 求めた確率が普通のフルーツなら
//    else{
//        fruitType = round(this->generateRandom(0, NOLMAL_FRUIT_COUNT - 1));
//    }
//    
//    // テクスチャのサイズを取り出す
//    auto textureSize = _fruitsBatchNode->getTextureAtlas()->getTexture()->getContentSize();
//    // テクスチャの横幅を個数で割るとフルーツ一つの幅になる
//    auto fruitWidth = textureSize.width / static_cast<int>(FruitsType::COUNT);
//    Sprite* fruit = Sprite::create("fruits.png",Rect(fruitWidth * fruitType,
//                                                  0,
//                                                  fruitWidth,
//                                                  textureSize.height));
//    fruit->setTag(fruitType);  // フルーツの種類をタグとして指定
//    
//    auto fruitSize = fruit->getContentSize();   //フルーツのサイズを取得
//    float fruitXPos = rand() % static_cast<int>(winSize.width);    //X軸のランダムな位置を計算
//
//    float fFruitYPos = winSize.height - FRUIT_TOP_MARGIN - fruitSize.height / 2.0f;
//    fruit->setPosition(Vec2(fruitXPos, fFruitYPos));
//    
//    // フルーツが生成された場所からエリアを決定
//    auto ariaType = GetAriaType(fruitXPos);
//    
//    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
//    ariaData[(int)ariaType].FruitType[fruitType] += 1;
//    
//    // BatchNodeにフルーツを追加
//    _fruitsBatchNode->addChild(fruit);
//    _fruits.pushBack(fruit);    // ベクター配列にフルーツを追加
//    // -------------------------------------
//    
//    // フルーツに動きをつける ------------------
////    // 地面の座標
////    auto ground = Vec2(fruitXPos,0);
////    //三秒かけて地面の位置まで落下アクション
////    auto fall = MoveTo::create(3,ground);
////    
////    // removeFruitを即座に呼びだづアクション
////    auto remove = CallFuncN::create([this](Node *node){
////        //NodeをSpriteにダウンキャスト
////        auto sprite = dynamic_cast<Sprite *>(node);
////        
////        // removeFruitを呼びだす
////        this->removeFruit(sprite);
////    });
////    
////    // fallとremoveを連続して実行するアクション
//////    auto sequence = Sequence::create(fall,remove,NULL);
//////    fruit->runAction(sequence);
////    
////    auto swing = Repeat::create(Sequence::create(RotateTo::create(0.25, -30), RotateTo::create(0.25, 30), NULL),2);
////    fruit->setScale(0);
////    fruit->runAction(Sequence::create(ScaleTo::create(0.25, 1),
////                                      swing,
////                                      RotateTo::create(0,0.125),
////                                      fall,
////                                      remove,
////                                      NULL));
//    // -------------------------------------
//    
//    return fruit;
//}

//// フルーツの削除
//bool MainScene::removeFruit(cocos2d::Sprite *fruit)
//{
//    // 当たった場所からエリアを決定
//    auto ariaType = GetAriaType( fruit->getPosition().x );
//    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
//    ariaData[(int)ariaType].FruitType[fruit->getTag()] -= 1;
//    
//    // _fruits(フルーツを格納している配列)にfruit(引数に入っていきたフルーツ)が含まれているか
//    if(_fruits.contains(fruit)){
//        // 親のノードから削除
//        fruit->removeFromParent();
//        // _fruits配列から削除
//        _fruits.eraseObject(fruit);
//        return true;
//    }
//    return false;
//}

// 更新
void MainScene::update(float dt)
{
    if(_state == GameState::PLAYING){
        // スコアの更新
        _scoreLabel->setString(StringUtils::toString(_score));
//        //GetTopAriaType();
//        // フルーツの出現
//        float pastTime = TIME_LIMIT_SECOND - _second;
//        float p = FRUIT_SPAWN_INCREASE_BASE * (1 + powf(FRUIT_SPAWN_INCREASE_RATE, pastTime));
//        p = MIN(p, MAXMUM_SPAWN_PROBABILITY);
//        //p = 0.01;
//        float random = this->generateRandom(0, 1);
//        if(random < p){
//            this->addFruit();
//        }
//        
//        for(auto& fruit : _fruits){
//            // フルーツの移動
//            auto pos = FruitMove( fruit );
//            fruit->setPosition(pos);
//            
//            // 当たり判定
//            Vec2 busketPosition = _player->getPosition() - Vec2(0,10);
//            // Spriteの描画範囲を取得
//            Rect boundingBox = fruit->getBoundingBox();
//            bool isHit = boundingBox.containsPoint(busketPosition);
//            if(isHit){
//                this->catchFruit(fruit);
//            }
//        }
        
        
        // 残り秒数を減らす
        _second -= dt;
        // 残り秒数の表示を更新する
        int second = static_cast<int>(_second);
        _secondLabel->setString(StringUtils::toString(second));
        
        if(_second < 0){    // 制限時間が０になったら
            // エンディングに移行
            _state = GameState::ENDING;
            //this->onResult();
            
            // 終了文字の表示
            auto finish = Sprite::create("finish.png");
            auto winSize = Director::getInstance()->getWinSize();
            finish->setPosition(Vec2(winSize.width / 2.0, winSize.height / 2.0));
            finish->setScale(0);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("finish.mp3");
            
            // アクションの作成
            auto appear = EaseExponentialIn::create(ScaleTo::create(0.25, 1.0));
            auto disappear = EaseExponentialIn::create(ScaleTo::create(0.25, 0));
            
            finish->runAction(Sequence::create(appear,
                                               DelayTime::create(2.0),
                                               disappear,
                                               DelayTime::create(1.0),
                                               CallFunc::create([this]{
                                                    _state = GameState::RESULT;
                                                    this->onResult();
                                               }),
                                               NULL));
            this->addChild(finish);
        }
        
        
//        // AIの移動
//        // 現在一番点数が高いエリアを取得
//        auto TopAriaType = GetTopAriaType();
//        if( bIsAIAriaMove ){
//            AIMove(fAriaSizeList[ (int)TopAriaType ]);
//            // 現在いるエリアを更新
//            CurrentAriaType = TopAriaType;
//            //log("エリア");
//        }
//        else{
//            AIMove( FruitDataListAnalysis().x );
//            //log("エリア内");
//        }
        
    }
}

// フルーツに当たったら
//void MainScene::catchFruit(cocos2d::Sprite *fruit){
//    
//    // もしクラッシュしていたら、フルーツを取得できない
//    if(this->getIsCrash()){
//        return;
//    }
//    
//    auto audioEngin = CocosDenshion::SimpleAudioEngine::getInstance();
//    // フルーツのタイプを取得
//    FruitsType fruittype = static_cast<FruitsType>(fruit->getTag());
//    
//    switch (fruittype) {
//        // 黄金フルーツの場合
//        case MainScene::FruitsType::GOLDEN:
//            _score += GOLDEN_FRUIT_SCORE;
//            audioEngin->playEffect("catch_golden.mp3");
//            
//            break;
//            
//        // 爆弾の場合
//        case MainScene::FruitsType::BOMB:
//            this->onCrashBomb();
//            audioEngin->playEffect("catch_bomb.mp3");
//            break;
//            
//        // それ以外のフルーツの場合
//        default:
//            _score += 1;
//            audioEngin->playEffect("catch_fruit.mp3");
//            break;
//    }
//    
////    // 当たった場所からエリアを決定
////    auto ariaType = GetAriaType( fruit->getPosition().x );
////    // フルーツ生成と同時に生成したフルーツに対応したデータを加算
////    ariaData[(int)ariaType].FruitType[(int)fruittype] -= 1;
//    
//    // フルーツの削除
//    this->removeFruit(fruit);
//    _scoreLabel->setString(StringUtils::toString(_score));
//}

// リザリトに移行
void MainScene::onResult(){
    //_state = GameState::RESULT;
    auto winSize = Director::getInstance()->getWinSize();
    
    // もう一度遊ぶボタン
    auto replayButton = MenuItemImage::create("replay_button.png","replay_button_pressed.png", [](Ref* ref){
        // もう一度遊ぶボタンが押された時の処理
        // 新しくSceneを作成
        auto scene = MainScene::createScene();
        // フェードアウトしながらシーン遷移
        auto transition = TransitionFade::create(0.5, scene);
        // シーン遷移の実行
        Director::getInstance()->replaceScene(transition);
    });
    
    // タイトルに戻るボタン
    auto titleButton = MenuItemImage::create("title_button.png", "title_button_pressed.png", [](Ref* ref){
        // タイトルに戻るボタンが押された時
        auto scene = TitleScene::createScene();
        auto transition = TransitionCrossFade::create(1.0, scene);
        Director::getInstance()->replaceScene(transition);
    });
    // 二つのボタンを使用しメニューを作成
    auto menu = Menu::create(replayButton, titleButton, NULL);
    // ボタンを縦に並べる
    menu->alignItemsVerticallyWithPadding(15);  // 引数でした数だけ間を空けて配置してくれる
    menu->setPosition(Vec2(winSize.width / 2.0, winSize.height / 2.0));
    this->addChild(menu);
}

// サウンドを鳴らす(トランジションが終了したタイミングで一度だけ呼ばれる)
void MainScene::onEnterTransitionDidFinish(){
    // シーン遷移が完了
    Layer::onEnterTransitionDidFinish();
    // BGMの再生
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("main.mp3", true);
    this->addReadyLabel();
}

// 爆弾に当たった時
//void MainScene::onCrashBomb()
//{
//    // クラッシュ状態にする
//    _isCrash = true;
//    
//    // アニメーションの再生
//    Vector<SpriteFrame *> frames;   // アニメーションの１コマずつ保存
//    auto playerSize = _player->getContentSize();
//    const int animationFrameCount = 3;  //アニメーションのフレーム数
//    
//    // アニメ用のフレームを読み込む
//    for(int i = 0; i < animationFrameCount; ++i){
//        // 画像のどの領域を扱うか設定（X、Y、幅、高さ）
//        auto rect = Rect(playerSize.width * i, 0, playerSize.width, playerSize.height);
//        auto frame = SpriteFrame::create("player_crash.png", rect);
//        frames.pushBack(frame);
//    }
//    
//    // アニメーションを作成
//    auto animation = Animation::createWithSpriteFrames(frames,10.0 / 60.0);
//    animation->setLoops(5); //三回繰り返して再生
//    animation->setRestoreOriginalFrame(true);   // アニメーションが終わった後に元の画像にもどすとtrue
//    _player->runAction(Sequence::create(Animate::create(animation), CallFunc::create([this]{_isCrash = false;}),NULL));
//    _score = MAX(0,_score - BOMB_PENALTY_SCORE);    // 第一引数と第二引数を比較して第一引数より小さかったら第一引数の値を代入
//    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("crash.mp3");
//    
//}

// ゲーム開始の文字
void MainScene::addReadyLabel()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto center = Vec2(winSize.width / 2.0, winSize.height / 2.0);
    
    // Readyの文字
    auto ready = Sprite::create("ready.png");
    ready->setScale(0);
    ready->setPosition(center);
    this->addChild(ready);
    
    // STARTの文字を定義
    auto start = Sprite::create("start.png");
    start->runAction(Sequence::create(CCSpawn::create(EaseIn::create(ScaleTo::create(0.5, 5.0), 0.5),
                                                      FadeOut::create(0.5), NULL),
                                                      RemoveSelf::create(), NULL));
    start->setPosition(center);
    
    // READYにアニメーションを追加
    ready->runAction(Sequence::create(ScaleTo::create(0.25, 1),
                                      DelayTime::create(1.0),
                                      CallFunc::create([this,start]{
        this->addChild(start);
        _state = GameState::PLAYING;
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("start.mp3");
    }),
                                      RemoveSelf::create(),
                                      NULL));
}

// 乱数を返す
//float MainScene::generateRandom(float min, float max)
//{
//    std::uniform_real_distribution<float> dest(min,max);
//    return dest(_engine);
//}

//// フルーツの移動
//Vec2 MainScene::FruitMove( cocos2d::Sprite* fruits )
//{
//    
//    auto FruitPos = fruits->getPosition();
//    //log("取得したフルーツの座標　X : %f Y : %f", FruitPos.x, FruitPos.y);
//    FruitPos.y -= 1;
//    
//    // フルーツの情報を構造体の変数にpushback : ここの処理は毎フレームやってしまうとまずいから改善する
//    FruitData fruitData;
//    fruitData.type = (FruitsType)fruits->getTag();
//    fruitData.pos = FruitPos;
//    fruitData.aria = GetAriaType(FruitPos.x);;
//    FruitDataList.push_back(fruitData);
//    
//    // Directorを取り出す
//    auto director = Director::getInstance();
//    // 画面サイズを取り出す
//    auto size = director->getWinSize();
//    
//    
//    // 画面から消えたフルーツを消す : ここの処理は毎フレームやってしまうとまずいから改善する
//    std::list<FruitData>::iterator fruitIterator;
//    fruitIterator = FruitDataList.begin();
//    while (fruitIterator != FruitDataList.end()) {
//        if(fruitData.pos.y <= size.height - 445){
//            fruitIterator = FruitDataList.erase(fruitIterator);
//        }else++fruitIterator;
//        
//    }
//
//    
//    //std::list<FruitData>::iterator fruitIterator;
//    //fruitIterator = FruitDataList.begin();
//    //fruitIterator++;
//    //FruitDataList.remove_if(fruitData.pos.y < size.height - 455 );
////    for( auto fruitData : FruitDataList ){
////        // フルーツが画面から消えたら
////        if(fruitData.pos.y <= size.height - 445){
////            //FruitDataList.remove(fruitData);
////            //log("通ってるよー");
////        }
////    }
//
//    
//    // 画面から消えたら
//    if(FruitPos.y < 0){
//        // removeFruitを即座に呼びだづアクション
//        auto remove = CallFuncN::create([this](Node *node){
//            //NodeをSpriteにダウンキャスト
//            auto sprite = dynamic_cast<Sprite *>(node);
//            
//            // removeFruitを呼びだす
//            this->removeFruit(sprite);
//        });
//        fruits->runAction(remove);
//    }
//    return FruitPos;
//}


// フルーツが生成された場所をAriaType型で取得
//MainScene::AriaType MainScene::GetAriaType( float x )
//{
//    // 最終的にreturnする値
//    AriaType aria;
//    
//    // 引数でもらったposと画面サイズを比較して対応したAriaTypeをいれる
//    // A
//    if(x < fAriaSizeList[(int)AriaType::A]){
//        aria = AriaType::A;
//    }
//    
//    // B
//    else if(x > fAriaSizeList[(int)AriaType::A] && x < fAriaSizeList[(int)AriaType::B]){
//        aria = AriaType::B;
//    }
//    
//    // C
//    else if(x > fAriaSizeList[(int)AriaType::B] && x < fAriaSizeList[(int)AriaType::C]){
//        aria = AriaType::C;
//    }
//    
//    return aria;
//}

// エリアごとの合計点数を調べ、一番高いエリアをAriaType型で返す
//MainScene::AriaType MainScene::GetTopAriaType()
//{
//    // 各エリアのスコアを取得して配列にする
//    int AriaScoreList[] = {
//        GetAriaScore( ariaData[(int)AriaType::A].FruitType ), // A
//        GetAriaScore( ariaData[(int)AriaType::B].FruitType ), // B
//        GetAriaScore( ariaData[(int)AriaType::C].FruitType )  // C
//    };
//    
//    log("点数A : %d, 点数B : %d, 点数C : %d", AriaScoreList[0],AriaScoreList[1],AriaScoreList[2]);
//    
//    // 一番スコアがいいエリアを決める
//    auto Top = MAX( (AriaScoreList[(int)AriaType::A]), (AriaScoreList[(int)AriaType::B]) );
//    auto TopScore = MAX(Top, AriaScoreList[(int)AriaType::C]);
//
//    // 点数の差の基準
//    const int difference = 5;
//    // 点数の差
//    auto scoreLenght = TopScore - AriaScoreList[(int)CurrentAriaType];
//    // 他のエリアとの差が大きかったらエリア移動のフラグをtrue
//    if( scoreLenght > difference ){
//        bIsAIAriaMove = true;
//    }
//    else{
//        bIsAIAriaMove = false;
//    }
//    
//    // 一番点数の高いエリアを入れる（初期はBにいるのでBを入れてる）
//    AriaType TopScoreAriaType = AriaType::B;
//    for( int i = 0; i < (int)AriaType::COUNT; i++ ){
//        // 一番高いスコアと一致したら
//        if( TopScore == AriaScoreList[i] ){
//            // AriaTypeは中身はintになってるのでiをそのまま代入
//            TopScoreAriaType = (AriaType)i;
//        }
//    }
//    
//    return TopScoreAriaType;
//}


//// 引数に入れた配列の中身を調べて合計点数を返す
//int MainScene::GetAriaScore( int score[] )
//{
//    
//    // エリアの点数
//    auto AriaScore = 0;
//    
//    // 種類別の点数を入れる
//    for( int i = 0; i < (int)FruitsType::COUNT; i++ ){
//        AriaScore += score[i];
//    }
//    return AriaScore;
//}

//// フルーツの情報をListから取得
//Vec2 MainScene::FruitDataListAnalysis()
//{
//    // フルーツの座標を保持する
//    Vec2 fruitPos = Vec2(0,100000);
//    
//    // fruitData分だけ
//    for( auto fruitData : FruitDataList ){
//        // フルーツの場所が今いるエリアだったら
//        if(fruitData.aria == CurrentAriaType){
//            //log("中身 : %f", fruitData.pos.y);
//            // 最小値を更新
//            if(fruitPos.y > fruitData.pos.y){
//                fruitPos = fruitData.pos;
//            }
//        }
//    }
//    
//    return fruitPos;
//}

//// AIの移動（目標となるposを入れる）
//void MainScene::AIMove( float targetPos )
//{
//    // 速度
//    float speed = 0.0;
//    // プレイヤーの座標
//    auto pos = _player->getPosition();
//    
//    // 移動し終わるとtrue
//    bool bIsMoveEnd = false;
//    
//    // 目標の場所への方向
//    auto angle = pos.x - targetPos;
//    
//    if( pos.x == targetPos ){
//        bIsMoveEnd = true;
//    }
//
//    // 移動し終わってなかったら
//    if( !bIsMoveEnd ){
//        if(angle >= 0){
//            speed = -1.0;
//        }
//        else{
//            speed = 1.0;
//        }
//        
//        // 移動
//        pos.x += speed;
//    }
//    else{
//        pos.x = targetPos;
//        speed = 0.0;
//    }
//
//    _player->setPosition(pos);
//}

// MainSceneのシングルトン
MainScene *MainScene::sharedMainScene()
{
    return instanceMainScene;
}


