//
//  Player.cpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#include "Player.hpp"
#include "cocos2d.h"
#include "MainScene.hpp"
#include "Fruits.hpp"
#include "GameData.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

// プレイヤーのシングルトン
static Player* instancePlayer;


// コンストラクタ
Player::Player()
: _player(NULL),
_isCrash(false)
{
    
}

// デストラクタ
Player::~Player()
{
    // プレイヤーの削除
    CC_SAFE_RELEASE_NULL(_player);
}

// 初期化
bool Player::init()
{
    if(!Layer::init()){
        return false;
    }
    
    // Directorを取り出す
    auto director = Director::getInstance();
    // 画面サイズを取り出す
    auto size = director->getWinSize();
    
    // プレイヤー
    this->setPlayer(Sprite::create("player.png"));  // スプライトを生成して格納
    _player->setPosition(Vec2(size.width / 2.0f, size.height - 445)); // プレイヤーの位置
    this->addChild(_player);    // シーンにプレイヤーを配置
    
    // プレイヤーのシングルトンを設定
    instancePlayer = this;
    
    // フルーツクラスのシングルトン取得
    fruits = Fruits::sharedFruits();
    
    // MainSceneクラスのシングルトン取得
    mainScene = MainScene::sharedMainScene();
    
    gameData = GameData::sharedGameData();
    
    
    // 更新
    this->scheduleUpdate();
    
    return true;
}

// 更新
void Player::update(float dt)
{
    // ゲームプレイ中
    if( mainScene->getState() == MainScene::GameState::PLAYING ){
        
        // 爆弾に当たっていたら
        auto crash = fruits->getCrashBomb();
        if(crash && !_isCrash){
            fruits->setCrashBomb(false);
            onCrashBomb();
            
        }
        
        // AIの移動
        // 現在一番点数が高いエリアを取得
        auto TopAriaType = fruits->GetTopAriaType();
        //log("%d", TopAriaType);
        if( fruits->bIsAIAriaMove ){
            //log("エリア移動");
            AIMove( fruits->fAriaSizeList[ (int)TopAriaType ] );
            // 現在いるエリアを更新
            fruits->CurrentAriaType = TopAriaType;
        }
        else{
            //log("移動");
            AIMove( fruits->FruitDataListAnalysis().x );
        }
        
        auto fruitData = fruits->getFruits();
        for(auto fruit : fruitData){
            // 当たり判定
            Vec2 busketPosition = _player->getPosition() - Vec2(0,10);
            // Spriteの描画範囲を取得
            Rect boundingBox = fruit->getBoundingBox();
            bool isHit = boundingBox.containsPoint(busketPosition);
            if(isHit){
                //log("当たり%d",a++);
                fruits->catchFruit(fruit);
            }
            else{
            }
            
        }


    }
}

// 爆弾に当たった時
void Player::onCrashBomb()
{
    // クラッシュ状態にする
    _isCrash = true;

    // アニメーションの再生
    Vector<SpriteFrame *> frames;   // アニメーションの１コマずつ保存
    auto playerSize = _player->getContentSize();
    const int animationFrameCount = 3;  //アニメーションのフレーム数

    // アニメ用のフレームを読み込む
    for(int i = 0; i < animationFrameCount; ++i){
        // 画像のどの領域を扱うか設定（X、Y、幅、高さ）
        auto rect = Rect(playerSize.width * i, 0, playerSize.width, playerSize.height);
        auto frame = SpriteFrame::create("player_crash.png", rect);
        frames.pushBack(frame);
    }

    // アニメーションを作成
    auto animation = Animation::createWithSpriteFrames(frames,10.0 / 60.0);
    animation->setLoops(5); //三回繰り返して再生
    animation->setRestoreOriginalFrame(true);   // アニメーションが終わった後に元の画像にもどすとtrue
    _player->runAction(Sequence::create(Animate::create(animation), CallFunc::create([this]{_isCrash = false;}),NULL));
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("crash.mp3");

}

// AIの移動（目標となるposを入れる）
void Player::AIMove( float targetPos )
{
    // 速度
    float speed = 0.0;
    // プレイヤーの座標
    auto pos = _player->getPosition();

    // 移動し終わるとtrue
    //bool bIsMoveEnd = false;

    // 目標の場所への方向
    auto angle = pos.x - targetPos;

//    if(targetPos <= 0){
//        log("pos : %f", targetPos);
//    }
    if( pos.x == targetPos ){
        bIsMoveEnd = true;
    }

    // 移動し終わってなかったら
    if( !bIsMoveEnd ){
        if(angle >= 0){
            speed = -1.0;
        }
        else{
            speed = 1.0;
        }

        // 移動
        pos.x += speed;
    }
    else{
        bIsMoveEnd = false;
        pos.x = targetPos;
        speed = 0.0;
    }

    _player->setPosition(pos);
}

// プレイヤーのシングルトンを取得
Player *Player::sharedPlayer()
{
    return  instancePlayer;
}