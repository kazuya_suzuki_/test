//
//  GameData.hpp
//  MyGame
//
//  Created by H1-200 on 2015/12/18.
//
//

#ifndef GameData_hpp
#define GameData_hpp

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;    // 書くとcocos2d::を省略できる(using namespece cocos2d と同じ記述)

// データをまとめたクラス
class GameData : public Layer
{
protected:
    // コンストタクタ
    GameData();
    // デストラクタ
    virtual ~GameData();
    // 初期化
    bool init() override;
    
public:
    CREATE_FUNC(GameData);
    
    //　フルーツの種類
    enum class FruitsType
    {
        APPLE = 0,  // りんご
        GRAPE,      // ぶどう
        ORANGE,     // オレンジ
        BANANA,     // バナナ
        CHERRY,     // さくらんぼ
        GOLDEN,     // 黄金のフルーツ
        BOMB,       // 爆弾
        COUNT       // 最大数
    };
    
    // 画面の種類
    enum class AriaType{
        A = 0,  // 右
        B,      // 真ん中
        C,      // 右
        COUNT   // エリア数
    };
    
//    // フルーツのデータをまとめた構造体
//    struct FruitData
//    {
//        FruitsType type;    // フルーツの種類
//        Vec2 pos;           // 生成された場所
//        AriaType aria;      // 生成されたエリア
//        int createNumber;   // 生成された順番
//    };
    
    // 各エリアごとの情報の構造体
    struct AriaData{
        AriaType type;  // エリアの種類
        int FruitType[(int)FruitsType::COUNT];   // エリアごとのフルーツを格納
    };
    
    // GameDataのシングルトンを取得
    static GameData *sharedGameData();
    
    // フルーツ用の配列を作成
    //CC_SYNTHESIZE(cocos2d::Vector<cocos2d::Sprite *>, _fruits, Fruits);
    
    // フルーツノード
    //CC_SYNTHESIZE_RETAIN(SpriteBatchNode *, _fruitsBatchNode, FruitsBatchNode);

};

#endif /* GameData_hpp */
