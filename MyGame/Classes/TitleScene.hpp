//
//  TitleScene.hpp
//  MyGame
//
//  Created by H1-200 on 2015/12/10.
//
//

#ifndef TitleScene_hpp
#define TitleScene_hpp

#include <stdio.h>
#include "cocos2d.h"

// タイトルシーン
class TitleScene : public cocos2d::Layer{
protected:
    TitleScene();
    virtual ~TitleScene();
    bool init() override;
  
public:
    static cocos2d::Scene* createScene();
    void onEnterTransitionDidFinish() override;
    CREATE_FUNC(TitleScene);
};

#endif /* TitleScene_hpp */
